-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 16-Maio-2022 às 13:50
-- Versão do servidor: 10.4.18-MariaDB
-- versão do PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `mwccrud`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE `cidade` (
  `COD_CIDADE` int(11) NOT NULL,
  `NOME_CIDADE` varchar(100) NOT NULL,
  `COD_ESTADO` int(11) NOT NULL,
  `DATA_CRIACAO_REGISTRO` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `cidade`
--

INSERT INTO `cidade` (`COD_CIDADE`, `NOME_CIDADE`, `COD_ESTADO`, `DATA_CRIACAO_REGISTRO`) VALUES
(1, 'Florianópolis', 1, '2022-05-15 22:12:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estado`
--

CREATE TABLE `estado` (
  `COD_ESTADO` int(11) NOT NULL,
  `NOME_ESTADO` varchar(100) NOT NULL,
  `DATA_CRIACAO_REGISTRO` timestamp NULL DEFAULT current_timestamp(),
  `COD_PAIS` int(11) NOT NULL,
  `SIGLA` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `estado`
--

INSERT INTO `estado` (`COD_ESTADO`, `NOME_ESTADO`, `DATA_CRIACAO_REGISTRO`, `COD_PAIS`, `SIGLA`) VALUES
(1, 'Santa Catarina', '2022-05-15 21:20:27', 1, 'SC'),
(3, 'Rio Grande Do Sul', '2022-05-15 22:12:47', 1, 'RS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `COD_FUNCIONARIO` int(11) NOT NULL,
  `NOME_FUNCIONARIO` varchar(255) NOT NULL,
  `ENDERECO` varchar(255) DEFAULT NULL,
  `BAIRRO` varchar(100) DEFAULT NULL,
  `COMPL_ENDERECO` varchar(100) DEFAULT NULL,
  `NUMERO_ENDERECO` varchar(45) DEFAULT NULL,
  `CEP` varchar(9) DEFAULT NULL,
  `CPF` varchar(14) DEFAULT NULL,
  `RG` varchar(45) DEFAULT NULL,
  `TELEFONE` varchar(45) DEFAULT NULL,
  `CELULAR` varchar(45) DEFAULT NULL,
  `DATA_NASCIMENTO` varchar(45) DEFAULT NULL,
  `EMAIL` varchar(45) DEFAULT NULL,
  `SALARIO` double DEFAULT NULL,
  `DATA_CRIACAO_REGISTRO` timestamp NOT NULL DEFAULT current_timestamp(),
  `COD_CIDADE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`COD_FUNCIONARIO`, `NOME_FUNCIONARIO`, `ENDERECO`, `BAIRRO`, `COMPL_ENDERECO`, `NUMERO_ENDERECO`, `CEP`, `CPF`, `RG`, `TELEFONE`, `CELULAR`, `DATA_NASCIMENTO`, `EMAIL`, `SALARIO`, `DATA_CRIACAO_REGISTRO`, `COD_CIDADE`) VALUES
(1, 'Daniel', 'sobe e desce', 'Centro', 'bl. 12 ', '65', '88080-250', '962.160.550-46', '732251463', '48 3028-8912', '48 98422-5717', '03/05/2022 19:26:01', 'daniel@gmail.com', 45112, '2022-05-15 22:45:24', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pais`
--

CREATE TABLE `pais` (
  `COD_PAIS` int(11) NOT NULL,
  `NOME_PAIS` varchar(100) NOT NULL,
  `DATA_CRIACAO_REGISTRO` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pais`
--

INSERT INTO `pais` (`COD_PAIS`, `NOME_PAIS`, `DATA_CRIACAO_REGISTRO`) VALUES
(1, 'Brasil', '2022-05-15 20:57:02'),
(3, 'Argentina', '2022-05-15 21:32:55'),
(4, 'Chile', '2022-05-15 21:43:43'),
(6, 'Uruguai', '2022-05-15 21:48:46'),
(7, 'Equador', '2022-05-15 21:50:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `COD_USER` int(11) NOT NULL,
  `NOME_USER` varchar(150) NOT NULL,
  `SENHA` longblob NOT NULL,
  `REGISTRO_ATIVO` char(1) NOT NULL DEFAULT 'V',
  `LOGIN` varchar(150) NOT NULL,
  `DATA_CRIACAO_REGISTRO` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`COD_USER`, `NOME_USER`, `SENHA`, `REGISTRO_ATIVO`, `LOGIN`, `DATA_CRIACAO_REGISTRO`) VALUES
(1, 'Daniel', 0x62343264333033313835326661326262306165653565383361353938323634343135306235646538353263353436363366313463363136383136303230626564, 'V', 'SUPER', '2022-05-15 18:32:13');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`COD_CIDADE`),
  ADD KEY `FK_CIDADE_ESTADO` (`COD_ESTADO`);

--
-- Índices para tabela `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`COD_ESTADO`),
  ADD KEY `FK_ESTADO_PAIS` (`COD_PAIS`);

--
-- Índices para tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`COD_FUNCIONARIO`),
  ADD KEY `FK_FUNCIONARIO_CIDADE` (`COD_CIDADE`);

--
-- Índices para tabela `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`COD_PAIS`);

--
-- Índices para tabela `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`COD_USER`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cidade`
--
ALTER TABLE `cidade`
  MODIFY `COD_CIDADE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `estado`
--
ALTER TABLE `estado`
  MODIFY `COD_ESTADO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `COD_FUNCIONARIO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `pais`
--
ALTER TABLE `pais`
  MODIFY `COD_PAIS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `user`
--
ALTER TABLE `user`
  MODIFY `COD_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `cidade`
--
ALTER TABLE `cidade`
  ADD CONSTRAINT `FK_CIDADE_ESTADO` FOREIGN KEY (`COD_ESTADO`) REFERENCES `estado` (`COD_ESTADO`);

--
-- Limitadores para a tabela `estado`
--
ALTER TABLE `estado`
  ADD CONSTRAINT `FK_ESTADO_PAIS` FOREIGN KEY (`COD_PAIS`) REFERENCES `pais` (`COD_PAIS`);

--
-- Limitadores para a tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD CONSTRAINT `FK_FUNCIONARIO_CIDADE` FOREIGN KEY (`COD_CIDADE`) REFERENCES `cidade` (`COD_CIDADE`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
