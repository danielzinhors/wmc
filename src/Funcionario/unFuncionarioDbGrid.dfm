inherited FuncionarioDbGrid: TFuncionarioDbGrid
  Caption = 'FuncionarioDbGrid'
  PixelsPerInch = 96
  TextHeight = 13
  inherited paBotoes: TPanel
    inherited ToolBar1: TToolBar
      ExplicitHeight = 60
    end
  end
  inherited paFooter: TPanel
    ExplicitLeft = 0
    ExplicitTop = 539
    ExplicitWidth = 867
    inherited btClose: TButton
      ExplicitLeft = 4
      ExplicitTop = 4
    end
  end
  inherited grGrid: TDBGrid
    Columns = <
      item
        Expanded = False
        FieldName = 'COD_FUNCIONARIO'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_FUNCIONARIO'
        Title.Caption = 'Nome'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SALARIO'
        Title.Caption = 'S'#225'lario'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TELEFONE'
        Title.Caption = 'Telefone'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CELULAR'
        Title.Caption = 'Celular'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EMAIL'
        Title.Caption = 'E-mail'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CPF'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RG'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENDERECO'
        Title.Caption = 'Endere'#231'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NUMERO_ENDERECO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'COMP_ENDERECO'
        Title.Caption = 'Complemento'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BAIRRO'
        Title.Caption = 'Bairro'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE'
        Title.Caption = 'Cidade'
        Visible = True
      end>
  end
end
