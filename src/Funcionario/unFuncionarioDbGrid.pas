unit unFuncionarioDbGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, unListaDbGrid, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ToolWin, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TFuncionarioDbGrid = class(TListaDbGrid)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FuncionarioDbGrid: TFuncionarioDbGrid;

implementation

uses
  unFuncionarioDbEdit, unFuncionarioDbDm;

{$R *.dfm}

procedure TFuncionarioDbGrid.FormCreate(Sender: TObject);
begin
  setDbDm(TFuncionarioDbDm);
  setDbEdit(TFuncionarioDbEdit);
  inherited;
  setTitulo('Funcionários');
end;

end.
