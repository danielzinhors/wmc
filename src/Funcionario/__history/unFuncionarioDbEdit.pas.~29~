unit unFuncionarioDbEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, unRegistroDbEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Mask, Vcl.Menus, Vcl.ComCtrls, System.types;

type
  TFuncionarioDbEdit = class(TRegistroDbEdit)
    dbNOME_FUNCIONARIO: TDBEdit;
    dbCPF: TDBEdit;
    dbRG: TDBEdit;
    dbTELEFONE: TDBEdit;
    dbEMAIL: TDBEdit;
    dbCELULAR: TDBEdit;
    dbENDERECO: TDBEdit;
    dbCOMPL_ENDERECO: TDBEdit;
    dbBAIRRO: TDBEdit;
    dbNUMERO_ENDERECO: TDBEdit;
    lbNOME_FUNCIONARIO: TLabel;
    lbCPF: TLabel;
    lbTELEFONE: TLabel;
    lbEMAIL: TLabel;
    lbRG: TLabel;
    lbCELULAR: TLabel;
    lbENDERECO: TLabel;
    lbCOMPL_ENDERECO: TLabel;
    lbBAIRRO: TLabel;
    lbNUMERO_ENDERECO: TLabel;
    lbCEP: TLabel;
    dbCEP: TDBEdit;
    lbCOD_CIDADE: TLabel;
    dbCOD_CIDADE: TDBLookupComboBox;
    btOpcoes: TButton;
    menuCidade: TPopupMenu;
    Adicionar1: TMenuItem;
    Adicionar2: TMenuItem;
    Editar1: TMenuItem;
    dsCidade: TDataSource;
    dtDATA_NASCIMENTO: TDateTimePicker;
    lbDATA_NASCIMENTO: TLabel;
    dbSALARIO: TDBEdit;
    lbSALARIO: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btOpcoesClick(Sender: TObject);
    procedure Adicionar1Click(Sender: TObject);
    procedure Editar1Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure dbCPFExit(Sender: TObject);
    procedure dbTELEFONEExit(Sender: TObject);
    procedure dbCELULARExit(Sender: TObject);
    procedure dbCEPExit(Sender: TObject);
    procedure dbSALARIOExit(Sender: TObject);
  private
    procedure abreCidades();
    procedure chamaEditorPais(prEditar: boolean = true);
  public
    procedure atualizaCombo(); override;
    procedure depoisDoInicio(); override;
  end;

var
  FuncionarioDbEdit: TFuncionarioDbEdit;

implementation

uses
  unFuncionarioDbDm, unCidadeDbEdit, DateUtils;

{$R *.dfm}

procedure TFuncionarioDbEdit.FormCreate(Sender: TObject);
begin
  setDbDm(TFuncionarioDbDm);
  inherited;
  setTitulo('Funcionário');
  //
  abreCidades();
  //
  dbSALARIO.editText := formatFloat('#,0.00;-#,0.00', 0);
end;

procedure TFuncionarioDbEdit.dbCELULARExit(Sender: TObject);
begin
  inherited;
  dbCELULAR.text := getUtils().getFone(dbCELULAR.text);
end;

procedure TFuncionarioDbEdit.dbCEPExit(Sender: TObject);
begin
  inherited;
  dbCEP.text := getUtils().getCep(dbCEP.text);
end;

procedure TFuncionarioDbEdit.dbCPFExit(Sender: TObject);
begin
  inherited;
  dbCPF.text := getUtils().getCPF(dbCPF.text);
end;

procedure TFuncionarioDbEdit.dbSALARIOExit(Sender: TObject);
begin
  inherited;
  dbSALARIO.text := formatFloat('#,0.00;-#,0.00', strToFloat(dbSALARIO.text));
end;

procedure TFuncionarioDbEdit.dbTELEFONEExit(Sender: TObject);
begin
  inherited;
  dbTELEFONE.text := getUtils().getFone(dbTELEFONE.text);
end;

procedure TFuncionarioDbEdit.depoisDoInicio();
begin
  if TFuncionarioDbDm(getDbDm()).fdQuery.fieldByName('DATA_NASCIMENTO').asString <> '' then
    dtDATA_NASCIMENTO.dateTime := TFuncionarioDbDm(getDbDm()).fdQuery.fieldByName('DATA_NASCIMENTO').asDateTime;
end;

procedure TFuncionarioDbEdit.atualizaCombo();
begin
  abreCidades();
end;

procedure TFuncionarioDbEdit.abreCidades();
begin
  TFuncionarioDbDm(getDbDm()).fdCidade.close;
  TFuncionarioDbDm(getDbDm()).fdCidade.open;
  dsCidade.dataSet := TFuncionarioDbDm(getDbDm()).fdCidade;
  dbCOD_CIDADE.refresh;
end;

procedure TFuncionarioDbEdit.btOpcoesClick(Sender: TObject);
begin
  inherited;
  with TButton(sender).clientToScreen(point(TButton(sender).width, TButton(sender).height)) do
    menuCidade.popup(X, Y);
end;

procedure TFuncionarioDbEdit.btSalvarClick(Sender: TObject);
begin
  if dtDATA_NASCIMENTO.date = date then
    addMsg('Informe a data de nascimento correta');
  if not getUtils().emailValido(dbEMAIL.text) then
    addMsg(getUtils().getMsg(true));
  if not getUtils().foneValido(dbTELEFONE.text) then
    addMsg(getUtils().getMsg(true));
  if not getUtils().CPFValido(dbCPF.text) then
    addMsg(getUtils().getMsg(true));
  if not getUtils().celularValido(dbCELULAR.text) then
    addMsg(getUtils().getMsg(true));
  if getMsg() <> '' then
  begin
    messageDlg(getMsg(true), mtError, [mbok], 0);
    exit;
  end;
  TFuncionarioDbDm(getDbDm()).fdQuery.fieldByName('DATA_NASCIMENTO').asDateTime := dtDATA_NASCIMENTO.dateTime;
  var stSalario := '';
  stSalario := dbSALARIO.text;
  getUtils().trocaString(stSalario, '.', '');
  getUtils().trocaString(stSalario, ',', '.');
  TFuncionarioDbDm(getDbDm()).fdQuery.fieldByName('SALARIO').asString := stSalario;
  inherited;
end;

procedure TFuncionarioDbEdit.chamaEditorPais(prEditar: boolean = true);
begin
  var dbEditLocal := TCidadeDbEdit.create2(self, getUtils());
  if prEditar then
  begin
    if (dbCOD_CIDADE.text) = '' then
    begin
      messageDlg('Selecione uma cidade para poder editar!', mtInformation, [mbok], 0);
      exit;
    end;
    dbEditLocal.alteraRegistro(dbCOD_CIDADE.listSource.dataSet.fieldByName('COD_CIDADE').asString);
  end
  else
    dbEditLocal.novoRegistro();
  dbEditLocal.show;
end;

procedure TFuncionarioDbEdit.Editar1Click(Sender: TObject);
begin
  inherited;
  chamaEditorPais();
end;

procedure TFuncionarioDbEdit.Adicionar1Click(Sender: TObject);
begin
  inherited;
  chamaEditorPais(false);
end;

end.
