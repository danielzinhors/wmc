inherited FuncionarioDbDm: TFuncionarioDbDm
  OnCreate = DataModuleCreate
  inherited fdQuery: TFDQuery
    UpdateOptions.UpdateTableName = 'FUNCIONARIO'
    UpdateOptions.KeyFields = 'COD_FUNCIONARIO'
    SQL.Strings = (
      'select F.*'
      'from FUNCIONARIO F'
      'order by F.COD_FUNCIONARIO')
  end
  object fdCidade: TFDQuery
    UpdateOptions.UpdateTableName = 'CIDADE'
    UpdateOptions.KeyFields = 'COD_CIDADE'
    SQL.Strings = (
      
        'select C.*, concat(C.NOME_CIDADE, '#39' - '#39', E.NOME_ESTADO, '#39' - '#39', P' +
        '.NOME_PAIS ) as NOME_CIDADE_ESTADO_PAIS'
      'from CIDADE C'
      '  left join ESTADO E on E.COD_ESTADO=C.COD_ESTADO'
      '  left join PAIS P on P.COD_PAIS=E.COD_PAIS'
      'order by P.COD_PAIS, E.COD_ESTADO, C.COD_CIDADE')
    Left = 120
    Top = 32
  end
end
