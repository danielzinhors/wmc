unit unFuncionarioDbDm;

interface

uses
  System.SysUtils, System.Classes, unDbDm, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFuncionarioDbDm = class(TDbDm)
    fdCidade: TFDQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FuncionarioDbDm: TFuncionarioDbDm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TFuncionarioDbDm.DataModuleCreate(Sender: TObject);
begin
  inherited;
  fdCidade.connection := getUtils().getConnection();
end;

end.
