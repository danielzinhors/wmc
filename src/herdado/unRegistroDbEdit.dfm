object RegistroDbEdit: TRegistroDbEdit
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  ClientHeight = 309
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object paBotoes: TPanel
    Left = 0
    Top = 268
    Width = 494
    Height = 41
    Align = alBottom
    TabOrder = 0
    object btSalvar: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 39
      Align = alLeft
      Caption = 'Salvar'
      TabOrder = 0
      OnClick = btSalvarClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 494
    Height = 268
    Align = alClient
    TabOrder = 1
  end
  object dsEdit: TDataSource
    Left = 296
    Top = 24
  end
  object tmInicio: TTimer
    Interval = 100
    OnTimer = tmInicioTimer
    Left = 160
    Top = 24
  end
end
