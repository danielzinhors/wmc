unit unDbDm;

interface

uses
  System.SysUtils, System.Classes, unUtils, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  DbDmClasse = class of TDbDm;

  TDbDm = class(TDataModule)
    fdQuery: TFDQuery;
  private
    utilsLocal: TUtils;
  public
    constructor create2(sender: TComponent; prUtils: TUtils);
     function salvaRegistro(): boolean;
     function getMsg(prLimpaMsg: boolean = false): String;
     function apaga(): boolean;
     function getUtils(): TUtils;
  end;

var
  DbDm: TDbDm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TPaiDbDm }

constructor TDbDm.create2(sender: TComponent; prUtils: TUtils);
begin
  utilsLocal := prUtils;
  inherited create(sender);
  fdQuery.connection := utilsLocal.getConnection();
  fdQuery.transaction := utilsLocal.getTransaction();
end;

function TDbDm.getMsg(prLimpaMsg: boolean = false): String;
begin
  result := utilsLocal.getMsg(prLimpaMsg);
end;

function TDbDm.getUtils(): TUtils;
begin
  result := utilsLocal;
end;

function TDbDm.salvaRegistro(): boolean;
begin
  try
    try
      fdQuery.post;
      result := true;
      except on e: Exception do
      begin
        utilsLocal.addMsg('Erro ao tentar salvar o registro: ' + e.message);
        result := false;
      end;
    end;
  finally
    fdQuery.close;
  end;
end;

function TDbDm.apaga(): boolean;
begin
  try
    fdQuery.delete;
    result := true;
    except on e: Exception do
    begin
      utilsLocal.addMsg('Erro ao tentar apagar o registro: ' + e.message);
      result := false;
    end;
  end;
end;

end.
