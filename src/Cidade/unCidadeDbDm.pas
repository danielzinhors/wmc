unit unCidadeDbDm;

interface

uses
  System.SysUtils, System.Classes, unDbDm, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TCidadeDbDm = class(TDbDm)
    fdEstado: TFDQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CidadeDbDm: TCidadeDbDm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TCidadeDbDm.DataModuleCreate(Sender: TObject);
begin
  inherited;
  fdEstado.connection := getUtils().getConnection();
end;

end.
