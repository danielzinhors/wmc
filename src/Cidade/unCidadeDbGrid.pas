unit unCidadeDbGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, unListaDbGrid, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ToolWin, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TCidadeDbGrid = class(TListaDbGrid)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CidadeDbGrid: TCidadeDbGrid;

implementation

uses
  unCidadeDbEdit, unCidadeDbDm;

{$R *.dfm}

procedure TCidadeDbGrid.FormCreate(Sender: TObject);
begin
  setDbDm(TCidadeDbDm);
  setDbEdit(TCidadeDbEdit);
  inherited;
  setTitulo('Cidades');
end;

end.
