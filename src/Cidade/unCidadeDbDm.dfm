inherited CidadeDbDm: TCidadeDbDm
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  inherited fdQuery: TFDQuery
    UpdateOptions.UpdateTableName = 'CIDADE'
    UpdateOptions.KeyFields = 'COD_CIDADE'
    SQL.Strings = (
      'select C.*, E.NOME_ESTADO, P.NOME_PAIS'
      'from CIDADE C'
      '  left join ESTADO E on E.COD_ESTADO=C.COD_ESTADO'
      '  left join PAIS P on P.COD_PAIS=E.COD_PAIS'
      'order by P.COD_PAIS, E.COD_ESTADO, C.COD_CIDADE')
  end
  object fdEstado: TFDQuery
    UpdateOptions.UpdateTableName = 'ESTADO'
    UpdateOptions.KeyFields = 'COD_ESTADO'
    SQL.Strings = (
      
        'select E.*, concat(E.NOME_ESTADO, '#39' - '#39',  P.NOME_PAIS) as NOME_E' +
        'STADO_PAIS '
      'from  ESTADO E '
      '   left join PAIS P on P.COD_PAIS=E.COD_PAIS'
      'order by P.COD_PAIS, E.COD_ESTADO')
    Left = 136
    Top = 32
  end
end
