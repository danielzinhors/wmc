inherited UserDbGrid: TUserDbGrid
  Caption = ''
  PixelsPerInch = 96
  TextHeight = 13
  inherited paFooter: TPanel
    ExplicitLeft = 0
    ExplicitTop = 539
    ExplicitWidth = 867
    inherited btClose: TButton
      ExplicitLeft = 4
      ExplicitTop = 4
    end
  end
  inherited grGrid: TDBGrid
    Columns = <
      item
        Expanded = False
        FieldName = 'COD_USER'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_USER'
        Title.Caption = 'Nome'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOGIN'
        Title.Caption = 'Login'
        Visible = True
      end>
  end
end
