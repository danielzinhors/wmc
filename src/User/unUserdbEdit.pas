unit unUserdbEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, unRegistroDbEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Mask, Vcl.DBCtrls;

type
  TUserDbEdit = class(TRegistroDbEdit)
    lbNOME_USER: TLabel;
    dbNOME_USER: TDBEdit;
    lbLOGIN: TLabel;
    dbLOGIN: TDBEdit;
    lbSENHA: TLabel;
    btMostraPass: TButton;
    edSenha: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure btMostraPassClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UserDbEdit: TUserDbEdit;

implementation

uses
  unUserDbDm;

{$R *.dfm}

procedure TUserDbEdit.FormCreate(Sender: TObject);
begin
  setDbDm(TUserDbDm);
  inherited;
  setTitulo('Usu�rio');
end;

procedure TUserDbEdit.btMostraPassClick(Sender: TObject);
begin
  inherited;
  if edSENHA.passwordChar = '*' then
    edSENHA.passwordChar := #0
  else
    edSENHA.passwordChar := '*';
end;

procedure TUserDbEdit.btSalvarClick(Sender: TObject);
begin
  if edSenha.text <> '' then
    TUserDbDm(getDbDm()).fdQuery.fieldByName('SENHA').asString := TUserDbDm(getDbDm()).getSenha(edSenha.text);
  inherited;
end;

end.
