inherited UserDbEdit: TUserDbEdit
  Caption = 'UserDbEdit'
  ClientHeight = 211
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited paBotoes: TPanel
    Top = 170
    ExplicitTop = 170
    ExplicitWidth = 494
    inherited btSalvar: TButton
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitHeight = 39
    end
  end
  inherited Panel2: TPanel
    Height = 170
    Caption = 'dbNOME_USER'
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 494
    ExplicitHeight = 170
    object lbNOME_USER: TLabel
      Left = 16
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Nome:'
      FocusControl = dbNOME_USER
    end
    object lbLOGIN: TLabel
      Left = 16
      Top = 64
      Width = 29
      Height = 13
      Caption = 'Login:'
      FocusControl = dbLOGIN
    end
    object lbSENHA: TLabel
      Left = 16
      Top = 104
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object dbNOME_USER: TDBEdit
      Left = 16
      Top = 29
      Width = 450
      Height = 21
      DataField = 'NOME_USER'
      DataSource = dsEdit
      TabOrder = 0
    end
    object dbLOGIN: TDBEdit
      Left = 16
      Top = 77
      Width = 450
      Height = 21
      DataField = 'LOGIN'
      DataSource = dsEdit
      TabOrder = 1
    end
    object btMostraPass: TButton
      Left = 432
      Top = 116
      Width = 34
      Height = 21
      ImageAlignment = iaCenter
      ImageIndex = 0
      TabOrder = 2
      OnClick = btMostraPassClick
    end
    object edSenha: TEdit
      Left = 16
      Top = 116
      Width = 410
      Height = 21
      PasswordChar = '*'
      TabOrder = 3
    end
  end
  inherited dsEdit: TDataSource
    Left = 240
    Top = 16
  end
end
