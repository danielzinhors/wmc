unit unPaisDbGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, unListaDbGrid, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ToolWin, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TPaisDbGrid = class(TListaDbGrid)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PaisDbGrid: TPaisDbGrid;

implementation

uses
  unPaisDbDm, unPaisDbEdit;

{$R *.dfm}

procedure TPaisDbGrid.FormCreate(Sender: TObject);
begin
  setDbDm(TPaisDbDm);
  setDbEdit(TPaisDbEdit);
  inherited;
  setTitulo('Paises');
end;

end.
