unit unPaisDbDm;

interface

uses
  System.SysUtils, System.Classes, unDbDm, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TPaisDbDm = class(TDbDm)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PaisDbDm: TPaisDbDm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
