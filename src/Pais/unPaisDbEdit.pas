unit unPaisDbEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, unRegistroDbEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Mask, Vcl.DBCtrls;

type
  TPaisDbEdit = class(TRegistroDbEdit)
    lbNOME_PAIS: TLabel;
    dbNOME_PAIS: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PaisDbEdit: TPaisDbEdit;

implementation

uses
  unPaisDbDm;

{$R *.dfm}

procedure TPaisDbEdit.FormCreate(Sender: TObject);
begin
  setDbDm(TPaisDbDm);
  inherited;
  setTitulo('Pais');
end;

end.
