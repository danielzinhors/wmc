inherited PaisDbDm: TPaisDbDm
  inherited fdQuery: TFDQuery
    UpdateOptions.UpdateTableName = 'PAIS'
    UpdateOptions.KeyFields = 'COD_PAIS'
    SQL.Strings = (
      'select P.*'
      'from PAIS P'
      'order by P.COD_PAIS')
  end
end
