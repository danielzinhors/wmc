inherited PaisDbGrid: TPaisDbGrid
  Caption = 'PaisDbGrid'
  PixelsPerInch = 96
  TextHeight = 13
  inherited paBotoes: TPanel
    inherited ToolBar1: TToolBar
      ExplicitHeight = 60
    end
  end
  inherited paFooter: TPanel
    ExplicitLeft = 0
    ExplicitTop = 539
    ExplicitWidth = 867
    inherited btClose: TButton
      ExplicitLeft = 4
      ExplicitTop = 4
    end
  end
  inherited grGrid: TDBGrid
    Columns = <
      item
        Expanded = False
        FieldName = 'COD_PAIS'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_PAIS'
        Title.Caption = 'Nome'
        Visible = True
      end>
  end
end
