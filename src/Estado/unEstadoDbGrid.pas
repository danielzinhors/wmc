unit unEstadoDbGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, unListaDbGrid, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ToolWin, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TEstadoDbGrid = class(TListaDbGrid)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EstadoDbGrid: TEstadoDbGrid;

implementation

uses
  unEstadoDbEdit, unEstadoDbDm;

{$R *.dfm}

procedure TEstadoDbGrid.FormCreate(Sender: TObject);
begin
  setDbDm(TEstadoDbDm);
  setDbEdit(TEstadoDbEdit);
  inherited;
  setTitulo('Estados');
end;

end.
