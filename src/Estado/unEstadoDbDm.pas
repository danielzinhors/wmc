unit unEstadoDbDm;

interface

uses
  System.SysUtils, System.Classes, unDbDm, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TEstadoDbDm = class(TDbDm)
    fdPais: TFDQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EstadoDbDm: TEstadoDbDm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TEstadoDbDm.DataModuleCreate(Sender: TObject);
begin
  inherited;
  fdPais.connection := getUtils().getConnection();
end;

end.
