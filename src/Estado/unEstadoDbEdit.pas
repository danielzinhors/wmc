unit unEstadoDbEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, unRegistroDbEdit, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Mask, Vcl.Menus, System.UiTypes, System.types;

type
  TEstadoDbEdit = class(TRegistroDbEdit)
    lbNOME_ESTADO: TLabel;
    lbSIGLA: TLabel;
    lbCOD_PAIS: TLabel;
    dbNOME_ESTADO: TDBEdit;
    dbSIGLA: TDBEdit;
    dbCOD_PAIS: TDBLookupComboBox;
    dsPais: TDataSource;
    btOpcoes: TButton;
    menuPais: TPopupMenu;
    Adicionar1: TMenuItem;
    Adicionar2: TMenuItem;
    Editar1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure Adicionar1Click(Sender: TObject);
    procedure Editar1Click(Sender: TObject);
    procedure btOpcoesClick(Sender: TObject);
  private
    procedure chamaEditorPais(prEditar: boolean = true);
  public
    procedure abrePais();
    procedure atualizaCombo(); override;
  end;

var
  EstadoDbEdit: TEstadoDbEdit;

implementation

uses
  unEstadoDbDm, unPaisDbEdit;

{$R *.dfm}

procedure TEstadoDbEdit.FormCreate(Sender: TObject);
begin
  setDbDm(TEstadoDbDm);
  inherited;
  setTitulo('Estado');
  //
  abrePais();
end;

procedure TEstadoDbEdit.abrePais();
begin
  TEstadoDbDm(getDbDm()).fdPais.close;
  TEstadoDbDm(getDbDm()).fdPais.open;
  dsPais.dataSet := TEstadoDbDm(getDbDm()).fdPais;
  dbCOD_PAIS.refresh;
end;

procedure TEstadoDbEdit.Adicionar1Click(Sender: TObject);
begin
  inherited;
  chamaEditorPais(false)
end;

procedure TEstadoDbEdit.atualizaCombo();
begin
  abrePais();
end;

procedure TEstadoDbEdit.btOpcoesClick(Sender: TObject);
begin
  inherited;
  with TButton(sender).clientToScreen(point(TButton(sender).width, TButton(sender).height)) do
    menuPais.popup(X, Y);
end;

procedure TEstadoDbEdit.chamaEditorPais(prEditar: boolean = true);
begin
  var dbEditLocal := TPaisDbEdit.create2(self, getUtils());
  if prEditar then
  begin
    if (dbCOD_PAIS.text) = '' then
    begin
      messageDlg('Selecione um pais para poder editar!', mtInformation, [mbok], 0);
      exit;
    end;
    dbEditLocal.alteraRegistro(dbCOD_PAIS.listSource.dataSet.fieldByName('COD_PAIS').asString);
  end
  else
    dbEditLocal.novoRegistro();
  dbEditLocal.show;
end;

procedure TEstadoDbEdit.Editar1Click(Sender: TObject);
begin
  inherited;
  chamaEditorPais();
end;

end.
