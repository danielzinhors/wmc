inherited EstadoDbDm: TEstadoDbDm
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  inherited fdQuery: TFDQuery
    UpdateOptions.UpdateTableName = 'ESTADO'
    UpdateOptions.KeyFields = 'COD_ESTADO'
    SQL.Strings = (
      'select E.*, P.NOME_PAIS'
      'from ESTADO E'
      '  left join PAIS P on P.COD_PAIS=E.COD_PAIS'
      'order by P.COD_PAIS, E.COD_ESTADO')
  end
  object fdPais: TFDQuery
    UpdateOptions.UpdateTableName = 'PAIS'
    UpdateOptions.KeyFields = 'COD_PAIS'
    SQL.Strings = (
      'select P.*'
      'from PAIS P'
      'order by P.COD_PAIS')
    Left = 120
    Top = 40
  end
end
