unit unAccessDb;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQLDef,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.MySQL, Data.DB,
  FireDAC.Comp.Client, Vcl.Forms, Inifiles;

type
  TAccessDb = class(TDataModule)
    fdConnection: TFDConnection;
    fdDriverMySql: TFDPhysMySQLDriverLink;
    fdWaitCursor: TFDGUIxWaitCursor;
    fdTransaction: TFDTransaction;
  private
    slMsg: TSTringList;
    procedure addMsg(prMsg: String);
  public
     function createConnection(): boolean;
     function getMsg(prLimpaMsg: boolean = false): String;
  end;

var
  AccessDb: TAccessDb;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TAccessDb }

procedure TAccessDb.addMsg(prMsg: String);
begin
  if slMsg = nil then
    slMsg := TStringList.create();
  if trim(prMsg) <> '' then
    slMsg.add(prMsg);
end;

function TAccessDb.createConnection(): boolean;
begin
  var iniFile := TInifile.create(extractFilePath(application.exeName) + 'config.ini');
  var stDbName := trim(iniFile.readString('CONFIG', 'DB_NAME', ''));
  var stDbUser := trim(iniFile.readString('CONFIG', 'DB_USER', ''));
  var stDbPassword := trim(iniFile.readString('CONFIG', 'DB_PASSWORD', ''));
  var stDbHost := trim(iniFile.readString('CONFIG', 'DB_HOST', 'localhost'));
//  var stDbCharset := trim(iniFile.readString('CONFIG', 'DB_CHARSET', 'UTF-8'));
//  var stDbCollate := trim(iniFile.readString('CONFIG', 'DB_COLLATE', ''));
  var stDbPort := trim(iniFile.readString('CONFIG', 'DB_PORT', ''));
  if (stDbName = '') or (stDbUser = '') then
  begin
    if stDbName = '' then
      addMsg('O nome da base de dados não foi encontrado!')
    else
      addMsg('O nome do usuário do banco não foi encontrado');
    result := false;
    exit;
  end;
  fdDriverMySql.vendorLib := extractFilePath(application.exeName) + '\utils\libs\mySql\32\libmysql.dll';
  fdConnection.params.driverId := 'MySQL';
  fdConnection.params.database := stDbName;
  fdConnection.params.values['server'] := stDbHost;
  fdConnection.params.values['port'] := stDbPort;
  fdConnection.params.userName := stDbUser;
  fdConnection.params.password := stDbPassword;
  fdConnection.params.
  try
    fdConnection.connected := true;
    result := true;
    except on e: Exception do
    begin
      addMsg(
        'Não foi possível conectar ao banco de dados. Erro: ' + e.message);
      result := false;
    end;
  end;
end;

function TAccessDb.getMsg(prLimpaMsg: boolean = false): String;
begin
  if slMsg = nil then
  begin
    result := '';
    exit;
  end;
  var stMsg := slMsg.text;
  if prLimpaMsg then
    slMsg.clear;
  result := stMsg;
end;

end.
