unit unUtils;

interface

uses
  System.SysUtils, System.Classes, unAccessDb, FireDAC.Comp.Client,
  System.ImageList, Vcl.ImgList, Vcl.Controls, Vcl.BaseImageCollection,
  Vcl.ImageCollection, System.RegularExpressions;

type
  TUtils = class(TDataModule)
    ilImgList: TImageList;
  private
    accessDbLocal: TAccessDb;
    slMsg: TSTringList;
     function zeroAEsquerda(prStr: string; prTamanho: integer): string;
     function getSoDigito(prStr: String): String;
    procedure retiraZerosAEsquerda(var prStr: String);
    function getStringSemAcento(prStr: String): String;
  public
     function conectDb(): boolean;
    procedure addMsg(prMsg: String);
     function getMsg(prLimpaMsg: boolean = false): String;
     function getConnection(): TFDConnection;
     function getTransaction(): TFDTransaction;
    procedure montaSql(prCd: TFDQuery; prFiltro: String);
     function getCPF(prCPF: String): String;
     function CPFValido(prNum: String): boolean;
     function getFone(prFone: String): String;
     function foneValido(prFone: String): boolean;
     function celularValido(prFone: String; prTestaDigito9: boolean = true): boolean;
     function emailValido(prEmail: String; prEmBrancoIsInvalido: boolean = true): boolean;
     function getCEP(prStr: String): String;
     function getStringComAspas(prStr: String): String;
     procedure trocaString(var prStrOrigem: String; prStr1, prStr2: String);
  end;

var
  Utils: TUtils;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TUtils }

function TUtils.conectDb(): boolean;
begin
  if accessDbLocal = nil then
    accessDbLocal := TAccessDb.create(self);
  var conectou := accessDbLocal.createConnection();
   if not conectou then
     addMsg(accessDbLocal.getMsg(true));
   result := conectou;
end;

procedure TUtils.addMsg(prMsg: String);
begin
  if slMsg = nil then
    slMsg := TStringList.create();
  if trim(prMsg) <> '' then
    slMsg.add(prMsg);
end;

function TUtils.getConnection(): TFDConnection;
begin
  result := accessDbLocal.fdConnection;
end;

function TUtils.getMsg(prLimpaMsg: boolean = false): String;
begin
  if slMsg = nil then
  begin
    result := '';
    exit;
  end;
  var stMsg := slMsg.text;
  if prLimpaMsg then
    slMsg.clear;
  result := stMsg;
end;

function TUtils.getTransaction(): TFDTransaction;
begin
  result := accessDbLocal.fdTransaction;
end;

procedure TUtils.montaSql(prCd: TFDQuery; prFiltro: String);

  procedure colocaFiltro();
  begin
    var indNovo := -1;
    var st := '';
    for var ind := 0 to prCd.sql.count - 1 do
    begin
      st := lowercase(prCd.sql[ind]);
      if (pos('where ', st) > 0) then
      begin
        if (pos(' where ', st) > 0) then
          continue;
        indNovo := ind;
        break;
      end
      else
      if (pos('order by', st) > 0) or (pos('group by', st) > 0) then
      begin
        indNovo := ind;
        prCd.sql.insert(indNovo, 'where 1=1');
        break;
      end
    end;
    if indNovo = -1 then
    begin
      indNovo := prCd.sql.count;
      prCd.sql.insert(indNovo, 'where 1=1');
    end;
    st := ' and ' + prFiltro;
    prCd.sql.insert(indNovo + 1, st);
  end;

  procedure colocaOrderBy();
  begin
    var st := prCd.sql.text;
    var posIni := pos('order by ', lowerCase(st));
    if posIni > 0 then
      delete(st, posIni, length(st));
    prCd.sql.text := st + ' ' + prFiltro;
  end;

begin
  if prFiltro = '' then
    exit;
  //
  prCd.close;
  //
  if pos('order by', lowerCase(prFiltro)) > 0 then
    colocaOrderBy()
  else
    colocaFiltro();
end;

function TUtils.getCPF(prCPF: String): String;
begin
  var st := getSoDigito(prCPF);
  st := zeroAEsquerda(st, 11);
  var stCpf :=
    copy(st, 1, 3) + '.' +
    copy(st, 4, 3) + '.' +
    copy(st, 7, 3) + '-' +
    copy(st, 10, 2);
  if stCpf = '000.000.000-00' then
    stCpf := '';
  result := stCpf;
end;

function TUtils.getSoDigito(prStr: String): String;
begin
  var st := '';
  for var ind := 1 to length(prStr) do
    if charInSet(prStr[ind], ['0'..'9']) then
      st := st + prStr[ind];
  result := st;
end;

function TUtils.zeroAEsquerda(prStr: string; prTamanho: integer): string;
begin
  var total := length(prStr);
  var st := prStr;
  if prTamanho > total Then
  begin
    var qtdeZero := prTamanho - total;
    st := '';
    for var ind := 1 to qtdeZero do
      st := st + '0';
    st := st + prStr;
  end
  else
  if total > prTamanho then
    while length(st) > prTamanho do
      delete(st, length(st), 1);
  result := st;
end;

function TUtils.CPFValido(prNum: String): boolean;
begin
  if trim(prNum) = '' then
  begin
    addMsg('CPF em branco');
    result := false;
    exit;
  end;
  //
  prNum := getSoDigito(prNum);
  if (prNum = '11111111111') or
     (prNum = '22222222222') or
     (prNum = '33333333333') or
     (prNum = '44444444444') or
     (prNum = '55555555555') or
     (prNum = '66666666666') or
     (prNum = '77777777777') or
     (prNum = '88888888888') or
     (prNum = '99999999999') then
  begin
    addMsg('CPF com n�meros iguais � inv�lido!');
    result := false;
    exit;
  end;
  //
  var st := prNum;
  retiraZerosAEsquerda(st);
  if trim(st) = '' then
  begin
    addMsg('CPF em branco');
    result := false;
    exit;
  end;
  //
  if length(prNum) < 11 then
  begin
    addMsg('CPF tem que ter 11 d�gitos.');
    result := false;
    exit;
  end;
  //
  var n1 := strToInt(prNum[1]);
  var n2 := strToInt(prNum[2]);
  var n3 := strToInt(prNum[3]);
  var n4 := strToInt(prNum[4]);
  var n5 := strToInt(prNum[5]);
  var n6 := strToInt(prNum[6]);
  var n7 := strToInt(prNum[7]);
  var n8 := strToInt(prNum[8]);
  var n9 := strToInt(prNum[9]);
  var d1 := n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;
  d1 := 11 - (d1 mod 11);
  if d1 >= 10 then
    d1 := 0;
  var d2 := d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;
  d2 := 11 - (d2 mod 11);
  if d2 >= 10 then
    d2 := 0;
  var calculado := inttostr(d1) + inttostr(d2);
  var digitado := prNum[10] + prNum[11];
  //
  if calculado = digitado then
    result := true
  else
  begin
    addMsg('CPF est� incorreto!');
    result := false;
  end;
end;

procedure TUtils.retiraZerosAEsquerda(var prStr: String);
begin
  if prStr = '' then
    exit;
  while prStr[1] = '0' do
  begin
    delete(prStr, 1, 1);
    if prStr = '' then
      break;
  end;
end;

function TUtils.getFone(prFone: String): String;
begin
  var st := trim(prFone);
  if st = '' then
  begin
    result := '';
    exit;
  end;
  //
  var stAux := st;
  trocaString(stAux, ' ', '');
  var isInter := (pos('+', stAux) > 0) and (copy(stAux, 2, 2) <> '55');
  if isInter then
  begin
    result := st;
    exit;
  end;
  st := getSoDigito(st);
  retiraZerosAEsquerda(st);
  if st = '' then
  begin
    result := '';
    exit;
  end;
  //
  if length(st) > 11 then
    st := copy(st, 1, 11);
  //
  if length(st) = 11 then
    result := copy(st,1,2) + ' ' + copy(st, 3, 5) + '-' + copy(st, 8, 4)
  else
  if length(st) = 10 then
    result := copy(st,1,2) + ' ' + copy(st,3,4) + '-' + copy(st,7,4)
  else
  if length(st) = 9 then
    result := copy(st, 1, 5) + '-' + copy(st, 6, 4)
  else
  if length(st) = 8 then
    result := copy(st, 1, 4) + '-' + copy(st, 5, 4)
  else
  if length(st) = 7 then
    result := copy(st, 1, 3) + '-' + copy(st, 4, 4)
  else
    result := st;
end;

procedure TUtils.trocaString(var prStrOrigem: String; prStr1, prStr2: String);
begin
  prStrOrigem := stringReplace(prStrOrigem, prStr1, prStr2, [rfReplaceAll, rfIgnoreCase]);
end;

function TUtils.foneValido(prFone: String): boolean;
begin
  var st := trim(getSoDigito(prFone));
  if st = '' then
  begin
    result := false;
    exit;
  end;
  var valido := true;
  if (length(st) < 10) then
  begin
    addMsg('Telefone inv�lido (menos que 10 d�gitos): ' + st);
    valido := false;
  end
  else
  if (length(st) > 11) then
  begin
    addMsg('Telefone inv�lido (mais que 11 d�gitos): ' + st);
    valido := false;
  end;
  result := valido;
end;

function TUtils.celularValido(prFone: String; prTestaDigito9: boolean = true): boolean;
begin
  var stAux := prFone;
  trocaString(stAux, ' ', '');
  var isInter := (pos('+', stAux) > 0) and (copy(stAux, 2, 2) <> '55');
  if isInter then
  begin
    result := true;
    exit;
  end;
  var st := trim(getSoDigito(prFone));
  var valido := true;
  if st = '' then
  begin
    addmsg('Celular sem conte�do');
    valido := false;
  end;
  if valido then
  begin
    if prTestaDigito9 then
    begin
      if length(st) = 10 then
      begin
        addMsg('Falta o digito 9 no inicio');
        valido := false;
      end;
    end;
  end;
  if valido then
    if (length(st) < 10) or (length(st) > 11) then
    begin
      addMsg('O celular deve ter o DDD e o n�mero');
      valido := false;
    end;
  if valido then
  begin
    delete(st, 1, 2);
    if (st[1] <> '9') then
    begin
      addMsg('Celular deve iniciar com 9');
      valido := false;
    end;
  end;
  result := valido;
end;

function TUtils.emailValido(prEmail: String; prEmBrancoIsInvalido: boolean = true): boolean;
var
  st: String;
  valido: boolean;

  procedure msgErroEmail(prMsg: String);
  begin
    var stMsg := 'E-mail inv�lido (' + prMsg + ')';
    if st <> '' then
      stMsg := stMsg + ': ' + st;
    addMsg(stMsg);
    valido := false;
  end;

  function verificarEmBranco(): boolean;
  begin
    if prEmBrancoIsInvalido then
    begin
      msgErroEmail('Em branco');
      result := false;
    end
    else
      result := true;
  end;

begin
  valido := true;
  st := trim(prEmail);
  if st = '' then
    verificarEmBranco();
  //
  if valido then
  begin
    st := trim(prEmail);
    if st = '' then
      valido := verificarEmBranco();
  end;
  //
  if valido then
    if st = '@' then
      msgErroEmail('somente com @');
  //
  if valido then
    if pos('@', st) = 0 then
      msgErroEmail('falta @');
  //
  if valido then
  begin
    var st2 := getStringSemAcento(st);
    if st2 <> upperCase(st) then
      msgErroEmail('caracter inv�lido');
  end;
    //
  if valido then
    if (pos('<',st) > 0) or
       (pos('>',st) > 0) or
       (pos('(',st) > 0) or
       (pos(')',st) > 0) or
       (pos('?',st) > 0) or
       (pos('�',st) > 0) or
       (pos('%',st) > 0) or
       (pos('#',st) > 0) or
       (pos('�',st) > 0) or
       (pos('[',st) > 0) or
       (pos(']',st) > 0) or
       (pos('=',st) > 0) or
       (pos(' ',st) > 0) then
      msgErroEmail('caracter inv�lido');
  //
  if valido then
    if st[length(st)] = '.' then
      msgErroEmail('ponto no final');
  //
  if valido then
    if copy(st, length(st)-1, length(st)) = '.b' then
      msgErroEmail('falta .br');
  //
  if valido then
  begin
    var stRegex := '([\w\.\-]+)?\w+@[\w-]+(\.\w+){1,}';
    var aceita := TRegEx.isMatch(st, stRegex);
    if not aceita then
      msgErroEmail('Formata��o errada');
  end;
  //
  result := valido;
end;

function TUtils.getStringSemAcento(prStr: String): String;
begin
  prStr := ansiUpperCase(prStr);
  for var ind := 1 to length(prStr) do
    case prStr[ind] of
      '�' : prStr[ind] := 'A';
      '�' : prStr[ind] := 'A';
      '�' : prStr[ind] := 'A';
      '�' : prStr[ind] := 'A';
      '�' : prStr[ind] := 'A';
      '�' : prStr[ind] := 'E';
      '�' : prStr[ind] := 'E';
      '�' : prStr[ind] := 'E';
      '�' : prStr[ind] := 'E';
      '�' : prStr[ind] := 'I';
      '�' : prStr[ind] := 'I';
      '�' : prStr[ind] := 'O';
      '�' : prStr[ind] := 'O';
      '�' : prStr[ind] := 'O';
      '�' : prStr[ind] := 'O';
      '�' : prStr[ind] := 'O';
      '�' : prStr[ind] := 'U';
      '�' : prStr[ind] := 'U';
      '�' : prStr[ind] := 'C';
      '�' : prStr[ind] := 'N';
      '�' : prStr[ind] := 'Y';
      '�' : prStr[ind] := 'y';
      #39 : prStr[ind] := ' ';
      '�' : prStr[ind] := 'A';
      '�' : prStr[ind] := 'O';
      '`' : prStr[ind] := ' ';
      '�' : prStr[ind] := ' ';
      '�' : prStr[ind] := 'O';
      '&' : prStr[ind] := 'E';
      '�' : prStr[ind] := 'A';
    end;
  result := trim(prStr);
end;

function TUtils.getCEP(prStr: String): String;
begin
  var st := getSoDigito(prStr);
  if length(st) < 8 then
  begin
    for var ind := length(st) + 1 to 8 do
      st := st + '0';
  end
  else
  if length(st) > 8 then
    st := copy(st, 1, 8);
  result := copy(st, 1, 5) + '-' + copy(st, 6, 3);
end;

function TUtils.getStringComAspas(prStr: String): String;
begin
  var st := quotedStr(prStr);
  result := st;
end;

end.
