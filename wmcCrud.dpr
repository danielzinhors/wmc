program wmcCrud;

uses
  Vcl.Forms,
  unPrincipal in 'unPrincipal.pas' {Principal},
  unUtils in 'utils\unUtils.pas' {Utils: TDataModule},
  Vcl.Themes,
  Vcl.Styles,
  unAccessDb in 'utils\unAccessDb.pas' {AccessDb: TDataModule},
  unDbDm in 'src\herdado\unDbDm.pas' {DbDm: TDataModule},
  unUserDbDm in 'src\User\unUserDbDm.pas' {UserDbDm: TDataModule},
  unListaDbGrid in 'src\herdado\unListaDbGrid.pas' {ListaDbGrid},
  unUserDbGrid in 'src\User\unUserDbGrid.pas' {UserDbGrid},
  unRegistroDbEdit in 'src\herdado\unRegistroDbEdit.pas' {RegistroDbEdit},
  unUserdbEdit in 'src\User\unUserdbEdit.pas' {UserDbEdit},
  unFuncionarioDbDm in 'src\Funcionario\unFuncionarioDbDm.pas' {FuncionarioDbDm: TDataModule},
  unFuncionarioDbGrid in 'src\Funcionario\unFuncionarioDbGrid.pas' {FuncionarioDbGrid},
  unFuncionarioDbEdit in 'src\Funcionario\unFuncionarioDbEdit.pas' {FuncionarioDbEdit},
  unPaisDbDm in 'src\Pais\unPaisDbDm.pas' {PaisDbDm: TDataModule},
  unPaisDbGrid in 'src\Pais\unPaisDbGrid.pas' {PaisDbGrid},
  unPaisDbEdit in 'src\Pais\unPaisDbEdit.pas' {PaisDbEdit},
  unEstadoDbDm in 'src\Estado\unEstadoDbDm.pas' {EstadoDbDm: TDataModule},
  unEstadoDbGrid in 'src\Estado\unEstadoDbGrid.pas' {EstadoDbGrid},
  unEstadoDbEdit in 'src\Estado\unEstadoDbEdit.pas' {EstadoDbEdit},
  unCidadeDbDm in 'src\Cidade\unCidadeDbDm.pas' {CidadeDbDm: TDataModule},
  unCidadeDbGrid in 'src\Cidade\unCidadeDbGrid.pas' {CidadeDbGrid},
  unCidadeDbEdit in 'src\Cidade\unCidadeDbEdit.pas' {CidadeDbEdit};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TPrincipal, Principal);
  Application.Run;
end.
